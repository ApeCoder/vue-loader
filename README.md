# vue-loader

### 介绍
vue单文件模板的java解析器，可将vue单文件模板文件解析为require.js可加载的js文件。集成了[Solon](https://gitee.com/noear/solon) 的支持

在Solon系统中使用，当debug==1或者设置vue.mode=0时, .vue文件直接返回模板内容，由loader解析，否则，.vue文件响应时翻译为js文件
loader基于require.js，可兼容所有兼容require.js的js模块，如：vue2.x,vuex.2.x,vueRouter.2.x 等
loader在require.js 基础上扩展了vue静态模板文件支持，并且可以自动以后缀名判断加载器

loader详见：https://gitee.com/ApeCoder/require-vue

也可单独调用 VueManager.get('xxx.vue') 获取/static/xxx.vue文件的对应js代码

当需要在浏览器中使用vue调试工具调试时,在访问地址中添加?debug可启用调试,同时需要自行在static/js目录下添加vue.js(vuejs的完整版),如:http://localhost:8080/index.html?debug#/xxx
### 配置说明
- 本loader集成了solon,有配置项vue.mode=[0,1,2,3,4],vue.stream=true
当运行参数中有--debug=1时,vue.mode强制为0,默认值为1,stream默认为true
- mode越高，初始化时间越长，第一次响应时间越长（做了编译缓存,编译后再次调用使用缓存）
- vue.stream表示用流的方式向前端输出vue代码
### mode说明
- mode 0: 直接输出.vue源文件，由前端loader解析
- mode 1: 编译为js文件。vue中对应的template,script,style只做简单的压缩(去除注释，空格等)。
- mode 2: 编译为js文件。在1的基础上，对script做uglify(简单的代码混淆)
- mode 3: 编译为js文件。在1的基础上,把template编译成vue的render函数
- mode 4: 编译为js文件。把template编译成vue的render函数,对script做uglify(简单的代码混淆)
### 开始使用
在solon项目中引用
```xml
    <dependency>
        <groupId>com.palm</groupId>
        <artifactId>vue-loader</artifactId>
        <version>1.5</version>
    </dependency>
```
由于项目未发到中央仓库，需添加制品库到pom.xml中
```xml
    <distributionManagement>
        <repository>
            <id>palmxj-framewrok-central</id>
            <name>central</name>
            <url>https://palmxj-maven.pkg.coding.net/repository/framewrok/central/</url>
        </repository>
    </distributionManagement>
```
#### 使用示例

index.html
```html
<div id="app"></div>
<script src="js/loader.min.js" data-main="main.js"></script>
```
main.js
```javascript
//配置需要使用的模块，满足requirejs的规范即可
//可配置常用的vue插件，ui等
require.config({
    paths:{
        vuex:'https://cdn.bootcdn.net/ajax/libs/vuex/3.6.2/vuex.min',
        vueRouter:'https://cdn.bootcdn.net/ajax/libs/vue-router/3.5.3/vue-router.min'
    }
})
require(['vue','test.vue'],function(Vue,test){
    /** 注册动态组件
    Vue.component('MyCom', function (resolve) {
       require(['coms/mycom.vue'], resolve)
    })
     **/
    /** 注册路由
     let router = new VueRouter({
        routes: [
            {
                path: '/login',
                name: 'Login',
                component: (r) => require(['login.vue'], r)
            }
        ]
   })
   **/
    new Vue(test).$mount("#app");
})
```
test.vue
```vue
<template>
  <div>
    <button @click="hello">Hello</button>
    {{count}}
  </div>
</template>
<script>
// import xx from './xx.vue' //引用其他组件
// import { mapGetters } from 'vuex' //引用vuex
export default {
  name:'Hello',
  data(){
    return {
      count:0
    }
  },
  /**
   // 使用对象展开运算符将 getter 混入 computed 对象中
   computed: {
    ...mapGetters([
      'doneTodosCount',
      'anotherGetter',
      // ...
    ])
   }
   **/
  methods:{
    hello(){
      this.count++
    }
  }
}
</script>
```
test.vue（兼容写法）
```vue
<template>
  <div>
    <button @click="hello">Hello</button>
    {{count}}
  </div>
</template>
<script>
// var xx = require('./xx.vue')//引用其他组件
// 属性提取不能使用了
// var vuex=require('vuex')
// var mapGetters=vuex.mapGetters
// 对象展开运算符将不兼容,需要手动改写
// var getters=mapGetters(['a','b'])
// getters.someElse=function(){
//    return this.count
// }
// 还有更多的地方
export default {
  name:'Hello',
  data:function(){
    return {
      count:0
    }
  },
  /**
   computed:getters,
   */
  methods:{
    hello:function(){
      this.count++
    }
  }
}
</script>
```
### 注意
vue模板的script里的代码决定了对浏览器的兼容性。如上面使用了ES6的语法，在某些浏览器中可能不兼容(大部分浏览器是兼容的,浏览器兼容性看这里：https://caniuse.com/?search=es6)
