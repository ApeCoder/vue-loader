package com.palm.vue.demo;

import org.noear.solon.Solon;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.zip.DeflaterOutputStream;

public class DemoApp {
    private static String script="import Vue from 'vue'\n" +
            "console.log(Vue)\n" +
            "\n" +
            "export default {\n" +
            "  name: \"Login\",\n" +
            "  data: function () {\n" +
            "    return {\n" +
            "      title: '测试页面',\n" +
            "      showDlg:false,\n" +
            "      username:'',\n" +
            "      password: '',\n" +
            "      verify:'',\n" +
            "      loading: false,\n" +
            "      verifyUrl:'https://img1.baidu.com/it/u=2995143552,1363900887&fm=26&fmt=auto&t='+new Date().getTime(),\n" +
            "      form: {\n" +
            "        name: '',\n" +
            "        region: '',\n" +
            "        date1: '',\n" +
            "        date2: '',\n" +
            "        delivery: false,\n" +
            "        type: [],\n" +
            "        resource: '',\n" +
            "        desc: ''\n" +
            "      }\n" +
            "    };\n" +
            "  },\n" +
            "  methods: {\n" +
            "    updateVerify(){\n" +
            "     // this.verifyUrl='captcha?t='+new Date().getTime();\n" +
            "      this.verify=''\n" +
            "    },\n" +
            "    submit: function () {\n" +
            "      if (!this.password) {\n" +
            "        this.$Message(\"密码不能为空\");\n" +
            "        return;\n" +
            "      }\n" +
            "      if (!this.verify) {\n" +
            "        this.$Message(\"验证码不能为空\");\n" +
            "        return;\n" +
            "      }\n" +
            "      if (this.verify.length!=5) {\n" +
            "        this.$Message(\"验证码长度为5位\");\n" +
            "        return;\n" +
            "      }\n" +
            "      this.loading = true;\n" +
            "      setTimeout(()=>{\n" +
            "        this.loading=false\n" +
            "      },3000)\n" +
            "    },\n" +
            "    onSubmit() {\n" +
            "      console.log('submit!');\n" +
            "    }\n" +
            "  }\n" +
            "};";
    private static String html="<template>\n" +
            "  <div class=\"login-vue\">\n" +
            "    <div class=\"login-container\">\n" +
            "      <div class=\"login-content\">\n" +
            "        <div class=\"login-title\">{{ title }}</div>\n" +
            "        <div class=\"login-input\">\n" +
            "          <input type=\"text\" name=\"username\" v-model=\"username\" autocomplete=\"off\"/>\n" +
            "          <span class=\"placeholder\" :class=\"{fixed: username != '' && username != null}\">用户名</span>\n" +
            "        </div>\n" +
            "        <div class=\"login-password login-input\">\n" +
            "          <input type=\"password\" name=\"password\" v-model=\"password\" autocomplete=\"off\"/>\n" +
            "          <span class=\"placeholder\" :class=\"{fixed: password != '' && password != null}\">密码</span>\n" +
            "        </div>\n" +
            "        <div class=\"login-input\">\n" +
            "          <input type=\"text\" name=\"verify\" v-model=\"verify\" @keyup.enter=\"submit\" autocomplete=\"off\" style=\"width: 150px\"/>\n" +
            "          <span class=\"placeholder\" :class=\"{fixed: verify != '' && verify != null}\">验证码</span>\n" +
            "          <img style=\"width: 100px;height:40px;cursor: pointer;\" :src=\"verifyUrl\" @click=\"updateVerify\"/>\n" +
            "        </div>\n" +
            "        <div class=\"buttonDiv\">\n" +
            "          <Button :loading=\"loading\" block color=\"primary\" size=\"l\" @click=\"submit\">登录</Button>\n" +
            "        </div>\n" +
            "        <div class=\"buttonDiv\">\n" +
            "          <Button @click=\"showDlg=true\"  block color=\"primary\" size=\"l\">Element UI Demo</Button>\n" +
            "        </div>\n" +
            "      </div>\n" +
            "    </div>\n" +
            "    <Modal v-model=\"showDlg\">\n" +
            "      <div>\n" +
            "        <el-form ref=\"form\" :model=\"form\" label-width=\"80px\">\n" +
            "          <el-form-item label=\"活动名称\">\n" +
            "            <el-input v-model=\"form.name\"></el-input>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item label=\"活动区域\">\n" +
            "            <el-select v-model=\"form.region\" placeholder=\"请选择活动区域\">\n" +
            "              <el-option label=\"区域一\" value=\"shanghai\"></el-option>\n" +
            "              <el-option label=\"区域二\" value=\"beijing\"></el-option>\n" +
            "            </el-select>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item label=\"活动时间\">\n" +
            "            <el-col :span=\"11\">\n" +
            "              <el-date-picker type=\"date\" placeholder=\"选择日期\" v-model=\"form.date1\" style=\"width: 100%;\"></el-date-picker>\n" +
            "            </el-col>\n" +
            "            <el-col class=\"line\" :span=\"2\">-</el-col>\n" +
            "            <el-col :span=\"11\">\n" +
            "              <el-time-picker placeholder=\"选择时间\" v-model=\"form.date2\" style=\"width: 100%;\"></el-time-picker>\n" +
            "            </el-col>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item label=\"即时配送\">\n" +
            "            <el-switch v-model=\"form.delivery\"></el-switch>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item label=\"活动性质\">\n" +
            "            <el-checkbox-group v-model=\"form.type\">\n" +
            "              <el-checkbox label=\"美食/餐厅线上活动\" name=\"type\"></el-checkbox>\n" +
            "              <el-checkbox label=\"地推活动\" name=\"type\"></el-checkbox>\n" +
            "              <el-checkbox label=\"线下主题活动\" name=\"type\"></el-checkbox>\n" +
            "              <el-checkbox label=\"单纯品牌曝光\" name=\"type\"></el-checkbox>\n" +
            "            </el-checkbox-group>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item label=\"特殊资源\">\n" +
            "            <el-radio-group v-model=\"form.resource\">\n" +
            "              <el-radio label=\"线上品牌商赞助\"></el-radio>\n" +
            "              <el-radio label=\"线下场地免费\"></el-radio>\n" +
            "            </el-radio-group>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item label=\"活动形式\">\n" +
            "            <el-input type=\"textarea\" v-model=\"form.desc\"></el-input>\n" +
            "          </el-form-item>\n" +
            "          <el-form-item>\n" +
            "            <el-button type=\"primary\" @click=\"onSubmit\">立即创建</el-button>\n" +
            "            <el-button @click=\"showDlg=false\">取消</el-button>\n" +
            "          </el-form-item>\n" +
            "        </el-form>\n" +
            "      </div>\n" +
            "    </Modal>\n" +
            "  </div>\n" +
            "</template>";
    private static String css=".login-vue {\n" +
            "  text-align: center;\n" +
            "  position: absolute;\n" +
            "  top: 0;\n" +
            "  bottom: 0;\n" +
            "  right: 0;\n" +
            "  left: 0;\n" +
            "  background: #f7f8fa;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container {\n" +
            "  width: 320px;\n" +
            "  position: absolute;\n" +
            "  top: 50%;\n" +
            "  left: 50%;\n" +
            "  transform: translate(-50%, -50%);\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content {\n" +
            "  letter-spacing: 2px;\n" +
            "  background: #FFF;\n" +
            "  padding: 70px 30px 20px;\n" +
            "  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.06);\n" +
            "  border-radius: 3px;\n" +
            "  box-sizing: border-box;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div {\n" +
            "  margin: 30px 0;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-input {\n" +
            "  position: relative;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-input .placeholder {\n" +
            "  position: absolute;\n" +
            "  color: #7e7e7e;\n" +
            "  top: 6px;\n" +
            "  font-size: 16px;\n" +
            "  transition: all .2s;\n" +
            "  left: 0;\n" +
            "  pointer-events: none;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-input input {\n" +
            "  font-size: 16px;\n" +
            "  padding: 8px 0;\n" +
            "  height: 40px;\n" +
            "  width: 100%;\n" +
            "  border: none;\n" +
            "  border-radius: 0;\n" +
            "  border-bottom: 1px solid #d3d3d3;\n" +
            "  box-shadow: inset 0 0 0 1000px #fff;\n" +
            "  outline: none;\n" +
            "  box-sizing: border-box;\n" +
            "  transition: .3s;\n" +
            "  font-weight: 200;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-input input:focus {\n" +
            "  border-bottom-color: #3788ee;\n" +
            "  box-shadow: inset 0 0 0 1000px #fff;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-input input:focus + .placeholder,\n" +
            ".login-vue .login-container .login-content > div.login-input .placeholder.fixed {\n" +
            "  font-size: 13px;\n" +
            "  top: -16px;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-input input:-webkit-autofill + .placeholder {\n" +
            "  font-size: 13px;\n" +
            "  top: -16px;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > div.login-title {\n" +
            "  font-size: 30px;\n" +
            "  color: #3a3a3a;\n" +
            "  line-height: 1;\n" +
            "  margin: -16px 0px 40px;\n" +
            "  font-weight: 200;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > .buttonDiv {\n" +
            "  margin-top: 45px;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > .buttonDiv .h-btn {\n" +
            "  padding: 12px 0;\n" +
            "  font-size: 18px;\n" +
            "  opacity: .8;\n" +
            "  border-radius: 3px;\n" +
            "  background: #3788ee;\n" +
            "  border-color: #3788ee;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .login-content > .buttonDiv .h-btn:hover {\n" +
            "  opacity: 1;\n" +
            "  background: #3788ee;\n" +
            "  border-color: #3788ee;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .copyright {\n" +
            "  letter-spacing: 1px;\n" +
            "  margin-top: 30px;\n" +
            "  color: #7e7e7e;\n" +
            "}\n" +
            "\n" +
            ".login-vue .login-container .copyright a {\n" +
            "  color: #7e7e7e;\n" +
            "}";
    public static void main(String[] args) throws Exception {
        Solon.start(DemoApp.class,args);
//        String rest=VueCompressor.compile(Utils.getResourceAsString("/static/login.vue"));
//        System.out.println(rest);
//        JavaScriptCompressor jc=new JavaScriptCompressor(new StringReader(rest),new ErrorReporter() {
//
//            public void warning(String message, String sourceName,
//                                int line, String lineSource, int lineOffset) {
//                System.err.println("\n[WARNING]");
//                if (line < 0) {
//                    System.err.println("  " + message);
//                } else {
//                    System.err.println("  " + line + ':' + lineOffset + ':' + message);
//                }
//            }
//
//            public void error(String message, String sourceName,
//                              int line, String lineSource, int lineOffset) {
//                System.err.println("[ERROR]");
//                if (line < 0) {
//                    System.err.println("  " + message);
//                } else {
//                    System.err.println("  " + line + ':' + lineOffset + ':' + message);
//                }
//            }
//
//            public EvaluatorException runtimeError(String message, String sourceName,
//                                                   int line, String lineSource, int lineOffset) {
//                error(message, sourceName, line, lineSource, lineOffset);
//                return new EvaluatorException(message);
//            }
//        });
//        StringWriter out=new StringWriter();
//        jc.compress(out,-1,false,false,false,false);
//        System.out.println(out);
        //        System.out.println();
//        System.out.println(ClosureCompressor.compileJs(rest));
//        System.out.println(rest.length());
//        System.out.println(Base64.getEncoder().encodeToString(rest.getBytes()).length());
//        System.out.println(zipBase64(rest));


    }
    /**
     * 压缩字符串,默认梳utf-8
     *
     * @param text
     * @return
     */
    public static String zipBase64(String text) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            try (DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(out)) {
                deflaterOutputStream.write(text.getBytes());
            }
            return new String(Base64.getEncoder().encodeToString(out.toByteArray()));
        } catch (IOException e) {
            System.err.println("压缩文本失败:"+e);
        }
        return "";
    }
}
