require.config({
    paths: {
        HeyUI: 'lib/heyui',
        ELEMENT: 'https://unpkg.com/element-ui/lib/index',
        axios:'lib/axios'
    },
    shim: {
        HeyUI: {
            deps: ['https://cdn.jsdelivr.net/npm/heyui/themes/index.css']
        },
        ELEMENT: {
            deps:['https://unpkg.com/element-ui/lib/theme-chalk/index.css']
        }
    }
})
define(['vue', 'HeyUI', 'login.vue', 'ELEMENT'], function (vue, heyui, login, element) {
    vue.use(heyui)
    vue.use(element)
    new vue(login).$mount("#app");
})
