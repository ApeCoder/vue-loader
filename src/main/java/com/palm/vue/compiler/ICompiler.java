package com.palm.vue.compiler;

import java.io.InputStream;

public interface ICompiler {
    String compile(String source);
    String compile(InputStream in);
}
