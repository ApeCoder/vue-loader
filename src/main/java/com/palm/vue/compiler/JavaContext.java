package com.palm.vue.compiler;

import java.util.Base64;

/**
 * 封装js和css的压缩，方便在java-js环境中调用
 */
public class JavaContext {
    public String compStyle(String style){
        return new CssCompressor(style).minify();
    }
    public String compScript(String script){
        return new JsCompressor(script).minify();
    }
    public String atob(String b){
        return new String(Base64.getDecoder().decode(b));
    }
    public String btoa(String b){
        return Base64.getEncoder().encodeToString(b.getBytes());
    }
}
