package com.palm.vue.compiler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlCompressor {
    private static final String tempPreBlock = "%%%HTMLCOMPRESS~PRE&&&";
    private static final String tempTextAreaBlock = "%%%HTMLCOMPRESS~TEXTAREA&&&";

    private static final Pattern commentPattern = Pattern.compile("<!--\\s*[^\\[].*?-->", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern itsPattern = Pattern.compile(">\\s+?<", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern prePattern = Pattern.compile("<pre[^>]*?>.*?</pre>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern taPattern = Pattern.compile("<textarea[^>]*?>.*?</textarea>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    public static String compress(String html){
        if(html == null || html.length() == 0) {
            return html;
        }
        List<String> preBlocks = new ArrayList<String>();
        List<String> taBlocks = new ArrayList<String>();

        String result = html;

        //preserve PRE tags
        Matcher preMatcher = prePattern.matcher(result);
        while(preMatcher.find()) {
            preBlocks.add(preMatcher.group(0));
        }
        result = preMatcher.replaceAll(tempPreBlock);

        //preserve TEXTAREA tags
        Matcher taMatcher = taPattern.matcher(result);
        while(taMatcher.find()) {
            taBlocks.add(taMatcher.group(0));
        }
        result = taMatcher.replaceAll(tempTextAreaBlock);

        //process pure html
        result = processHtml(result);

        //process preserved blocks
        result = processPreBlocks(result, preBlocks);
        result = processTextareaBlocks(result, taBlocks);

        preBlocks = taBlocks = null;

        return result.trim();
    }

    private static String processHtml(String html) {
        String result = html;

        //remove comments
        result = commentPattern.matcher(result).replaceAll("");

        //remove inter-tag spaces
        result = itsPattern.matcher(result).replaceAll("><");

        //remove multi whitespace characters
        result = result.replaceAll("\\s{2,}"," ");
        return result;
    }


    private static String processPreBlocks(String html, List<String> blocks)  {
        String result = html;
        //put preserved blocks back
        while(result.contains(tempPreBlock)) {
            result = result.replaceFirst(tempPreBlock, Matcher.quoteReplacement(blocks.remove(0)));
        }
        return result;
    }

    private static String processTextareaBlocks(String html, List<String> blocks) {
        String result = html;
        //put preserved blocks back
        while(result.contains(tempTextAreaBlock)) {
            result = result.replaceFirst(tempTextAreaBlock, Matcher.quoteReplacement(blocks.remove(0)));
        }
        return result;
    }










}
